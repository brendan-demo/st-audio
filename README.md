# Stranger Things Audio Decoding

In the first part of Season 4, Stranger Things presented a phone number that they later connected to modem when they called it and realized it was a computer (NINA).

The phone number is

> +1 (202) 968-6161

And if [you call it](https://screenrant.com/stranger-things-nina-phone-number-call-explained/), it sounds like you're listening in to someone dialing the number and then hearing the opening modem tones.

My general theory is there HAS to be a deeper easter egg in this audio, right? Someone has already done some research and [disagress with me](https://www.reddit.com/r/StrangerThings/comments/v1i7y2/further_analysis_on_the_phone_numbers/)

## Folder Structure

```
   /source          The original source files
   /known_files     Know files for testing
   .                Audacity projects and wav files
```

## Tools

- [Audacity](https://www.audacityteam.org/)
- [multimon-ng](https://github.com/EliasOenal/multimon-ng)
- [minimodem](http://www.whence.com/minimodem/)
- [GNUradio](https://www.gnuradio.org/)

## Research

### multimon-ng

- [Audacity Decoding Data?! Using Audacity Multimon-ng and Minimodem to Decode Digital Audio Data!](https://www.youtube.com/watch?v=tKNNnbgoGdI)
- [Hack-A-Sat CTF Part 2](https://keramas.github.io/2020/05/25/HackASat-Part2.html)
- https://quals.2020.hackasat.com/challenges/25.html
- https://github.com/cromulencellc/hackasat-qualifier-2020/blob/master/README.md
- https://medium.com/@solomontan_68263/56k-flex-magic-hack-a-sat-2020-f63df73b7dfd
- https://afresearchlab.com/events/hack-a-sat-space-security-challenge-2020-event-1/

### Minimodem

- https://www.youtube.com/watch?v=pwuyMJfyNmY
- https://www.rtl-sdr.com/tag/minimodem/
- https://medium.com/poka-techblog/back-to-basics-decoding-audio-modems-with-audacity-c94faa8362a0
